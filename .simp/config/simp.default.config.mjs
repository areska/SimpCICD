import { defineConfig } from "simpcicd";
const defaultConfig = defineConfig({
  pipelines: [
    {
      name: "lint",
      steps: [
        {
          name: "linting",
          commands: ["pnpm lint"]
        }
      ]
    },
    {
      name: "build",
      steps: [
        {
          name: "pre-build",
          commands: ["rm -rf dist/*"]
        },
        {
          name: "build",
          commands: ["pnpm install", "pnpm build"]
        },
        {
          name: "bin files mode",
          "non-blocking": true,
          commands: ["chmod +x dist/bin/*.js", "chmod +x dist/bin/forker/*.js"]
        },
        {
          name: "explicit esm",
          commands: ["cp public/package.json dist"]
        }
      ]
    }
  ]
});
export { defaultConfig };
