# Simp CICD

Even the smallest projects need their CICD tools.

It is now refactored as [Pipelight](https://pipelight.dev)

## Is Archived but... REFACTORED

Simpcicd was a prototype... full of bugs...
But the idea of a convenient deployment tool has deeply matured since its launch.

It is now reborn as [Pipelight](https://pipelight.dev)

# Stop downloading this shit... UPGRADE TO PIPELIGHT!

<p align="center">Deployment made easy.</p>
<p align="center">
  <a href="https://npmcharts.com/compare/simpcicd?minimal=true">
  <img src="https://img.shields.io/npm/dm/simpcicd.svg?sanitize=true" alt="Downloads">
  </a>
  <a href="https://www.npmjs.com/package/simpcicd">
  <img src="https://img.shields.io/npm/v/simpcicd.svg?sanitize=true" alt="Version">
  </a>
  <a href="https://www.npmjs.com/package/simpcicd">
  <img src="https://img.shields.io/npm/l/simpcicd.svg?sanitize=true" alt="License">
  </a>
  <a href="https://discord.gg/swNRD3Xysz">
  <img src="https://img.shields.io/badge/chat-on%20discord-7289da.svg?sanitize=true" alt="Chat">
  </a>
</p>

Check the doc at [Pipelight](https://pipelight.dev)

<h3> 
Your usual shell script...
</h3>

```sh{3,5}
#simple_example.sh
## List files
ls;
## Get working directory
pwd;

```

<h3> 
...rewrited into a Pipeline...
</h3>

```ts
{
  name: "simple_example",
  steps: [
    {
      name: "list files",
      commands: ["ls"]
    },
    {
      name: "get working directory",
      commands: ["pwd"]
    }
  ]
}
```

<h3>
...gets automatic triggers and pretty logs.
</h3>

<img class="sexy" src="https://pipelight.dev/images/example_log_level_4.png" alt="example pretty verbose logs">
