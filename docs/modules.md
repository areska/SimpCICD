[simpcicd](README.md) / Modules

# simpcicd

## Table of contents

### Interfaces

- [Config](interfaces/Config.md)
- [ExecContext](interfaces/ExecContext.md)
- [ExecOptions](interfaces/ExecOptions.md)
- [Pipeline](interfaces/Pipeline.md)
- [Step](interfaces/Step.md)

### Type Aliases

- [Action](modules.md#action)

### Functions

- [call](modules.md#call)
- [defineConfig](modules.md#defineconfig)
- [fork](modules.md#fork)
- [getBranch](modules.md#getbranch)
- [getGitPath](modules.md#getgitpath)
- [useCli](modules.md#usecli)
- [useConfig](modules.md#useconfig)
- [useExec](modules.md#useexec)
- [useHooks](modules.md#usehooks)
- [useTrigger](modules.md#usetrigger)

## Type Aliases

### Action

Ƭ **Action**: ``"pre-commit"`` \| ``"pre-push"`` \| ``"pre-receive"`` \| ``"update"`` \| ``"post-receive"``

#### Defined in

def/types.ts:71

## Functions

### call

▸ **call**(`__namedParameters`): `Promise`<`void`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | `Args` |

#### Returns

`Promise`<`void`\>

#### Defined in

utils/caller.ts:12

___

### defineConfig

▸ **defineConfig**(`config`): [`Config`](interfaces/Config.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `config` | [`Config`](interfaces/Config.md) |

#### Returns

[`Config`](interfaces/Config.md)

#### Defined in

composables/config.ts:35

___

### fork

▸ **fork**(`__namedParameters`): `Promise`<`void`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | `ForkOptions` |

#### Returns

`Promise`<`void`\>

#### Defined in

utils/forker.ts:11

___

### getBranch

▸ **getBranch**(): `Promise`<`string`\>

#### Returns

`Promise`<`string`\>

#### Defined in

utils/git.ts:5

___

### getGitPath

▸ **getGitPath**(): `Promise`<`string`\>

#### Returns

`Promise`<`string`\>

#### Defined in

utils/git.ts:10

___

### useCli

▸ **useCli**(`config`): `CAC`

#### Parameters

| Name | Type |
| :------ | :------ |
| `config` | [`Config`](interfaces/Config.md) |

#### Returns

`CAC`

#### Defined in

resolvers/cli.ts:10

___

### useConfig

▸ **useConfig**(`config?`): `Promise`<[`Config`](interfaces/Config.md)\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `config?` | [`Config`](interfaces/Config.md) |

#### Returns

`Promise`<[`Config`](interfaces/Config.md)\>

#### Defined in

composables/config.ts:12

___

### useExec

▸ **useExec**(): `Object`

#### Returns

`Object`

| Name | Type |
| :------ | :------ |
| `exec` | (`cmd`: `string`, `opts?`: [`ExecOptions`](interfaces/ExecOptions.md)) => `Promise`<`unknown`\> |
| `execPipeline` | (`pipeline`: [`Pipeline`](interfaces/Pipeline.md)) => `Promise`<`void`\> |
| `execStep` | (`step`: [`Step`](interfaces/Step.md)) => `Promise`<`void`\> |

#### Defined in

composables/exec.ts:8

___

### useHooks

▸ **useHooks**(`config?`): `Object`

#### Parameters

| Name | Type |
| :------ | :------ |
| `config?` | [`Config`](interfaces/Config.md) |

#### Returns

`Object`

| Name | Type |
| :------ | :------ |
| `linkHooks` | (`config`: [`Config`](interfaces/Config.md)) => `Promise`<`void`\> |
| `makeHooks` | (`config`: [`Config`](interfaces/Config.md)) => `Promise`<`void`\> |
| `toHook` | (`target`: `string`) => `Promise`<`void`\> |

#### Defined in

resolvers/hooks.ts:10

___

### useTrigger

▸ **useTrigger**(`config`): `Object`

#### Parameters

| Name | Type |
| :------ | :------ |
| `config` | [`Config`](interfaces/Config.md) |

#### Returns

`Object`

| Name | Type |
| :------ | :------ |
| `bulkTrigger` | (`action`: [`Action`](modules.md#action)) => `Promise`<`unknown`\> |
| `trigger` | (`name?`: `string`, `options?`: `any`) => `Promise`<`unknown`\> |

#### Defined in

resolvers/trigger.ts:7
