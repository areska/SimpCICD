[simpcicd](../README.md) / [Modules](../modules.md) / Pipeline

# Interface: Pipeline

## Table of contents

### Properties

- [commands](Pipeline.md#commands)
- [name](Pipeline.md#name)
- [steps](Pipeline.md#steps)
- [trigger](Pipeline.md#trigger)

## Properties

### commands

• `Optional` **commands**: `string`[]

#### Defined in

def/types.ts:19

___

### name

• **name**: `string`

#### Defined in

def/types.ts:18

___

### steps

• **steps**: [`Step`](Step.md)[]

#### Defined in

def/types.ts:20

___

### trigger

• `Optional` **trigger**: `Trigger`

#### Defined in

def/types.ts:21
