[simpcicd](../README.md) / [Modules](../modules.md) / Config

# Interface: Config

## Table of contents

### Properties

- [pipelines](Config.md#pipelines)

## Properties

### pipelines

• **pipelines**: [`Pipeline`](Pipeline.md)[]

#### Defined in

def/types.ts:8
