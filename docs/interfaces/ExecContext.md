[simpcicd](../README.md) / [Modules](../modules.md) / ExecContext

# Interface: ExecContext

## Table of contents

### Properties

- [verbose](ExecContext.md#verbose)

## Properties

### verbose

• `Optional` **verbose**: `boolean`

#### Defined in

def/types.ts:50
