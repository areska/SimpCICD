[simpcicd](../README.md) / [Modules](../modules.md) / Step

# Interface: Step

## Table of contents

### Properties

- [commands](Step.md#commands)
- [name](Step.md#name)
- [non-blocking](Step.md#non-blocking)

## Properties

### commands

• **commands**: `string`[]

#### Defined in

def/types.ts:33

___

### name

• **name**: `string`

#### Defined in

def/types.ts:32

___

### non-blocking

• `Optional` **non-blocking**: `boolean`

#### Defined in

def/types.ts:31
