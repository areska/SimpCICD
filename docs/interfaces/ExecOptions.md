[simpcicd](../README.md) / [Modules](../modules.md) / ExecOptions

# Interface: ExecOptions

## Table of contents

### Properties

- [non-blocking](ExecOptions.md#non-blocking)

## Properties

### non-blocking

• `Optional` **non-blocking**: `boolean`

#### Defined in

def/types.ts:58
