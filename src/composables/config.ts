import type { Config, Pipeline, Action } from "@def/types";
import { cwd } from "process";
import { log } from "@composables/logger";
import lodash from "lodash";
import { getBranch } from "@utils/git";
import { lilconfig, LilconfigResult } from "lilconfig";
import path from "path";

interface Store {
  config: Config;
  root: string | null;
}

const store: Store = { config: {} as Config, root: null };

const useStore = () => store;

const useConfig = async (config?: Config) => {
  const set = async (config?: Config) => {
    if (!!config) {
      store.config = config;
      return store.config;
    }
    try {
      const esmLoader = (filepath: string) => import(filepath);
      const options = {
        loaders: {
          ".js": esmLoader,
          ".mjs": esmLoader
        },
        searchPlaces: [
          "simp.config.mjs",
          "simp.config.js",
          "simp.conf.mjs",
          "simp.conf.js"
        ]
      };
      const res: LilconfigResult = await lilconfig("simp", options).search();
      store.config = res!.config.default;
      store.root = path.dirname(res!.filepath);
      return store.config;
    } catch (err) {
      log.error(err);
      return;
    }
  };
  await set(config);
  return store.config;
};

/**
 * @param {Config} config
 * @returns {Config} config
 */
const defineConfig = (config: Config): Config => {
  return config;
};

const getActions = (config: Config): Action[] => {
  let actions: Action[] = [];
  for (const pipeline of config.pipelines) {
    if (!!pipeline.trigger?.actions) {
      actions = actions.concat(pipeline.trigger?.actions);
    }
  }
  actions = lodash.uniq(actions);
  return actions;
};

type reducerArgs = {
  name?: string;
  config: Config;
};
const reducerName = async ({ name, config }: reducerArgs): Promise<Config> => {
  config.pipelines = config.pipelines.filter((pipeline: Pipeline) => {
    if (pipeline.name == name) {
      return pipeline;
    } else {
      return;
    }
  });
  if (config.pipelines.length == 0) {
    log.debug(`couldn't find pipeline "${name}"`);
  }
  return config;
};
const hasNoTrigger = async ({ name, config }: reducerArgs): Promise<Config> => {
  config.pipelines = config.pipelines.filter((pipeline: Pipeline) => {
    const cond = !pipeline.trigger;
    if (cond) {
      return pipeline;
    } else {
      return;
    }
  });
  return config;
};
const hasTrigger = async ({ name, config }: reducerArgs): Promise<Config> => {
  config.pipelines = config.pipelines.filter((pipeline: Pipeline) => {
    const cond = !!pipeline.trigger;
    if (cond) {
      return pipeline;
    } else {
      return;
    }
  });
  return config;
};
const reducerBranch = async ({
  name,
  config
}: reducerArgs): Promise<Config> => {
  const actualBranch = await getBranch();
  if (config.pipelines.length == 0) {
    return config;
  }
  config.pipelines = config.pipelines.filter((pipeline: Pipeline) =>
    pipeline.trigger?.branches?.includes(actualBranch)
  );
  if (config.pipelines.length == 0) {
    log.debug(`checkout to permitted branch to trigger pipeline ${name}`);
  }
  return config;
};

type reducerActionArgs = {
  action: Action;
  config: Config;
};
const reducerAction = async ({
  action,
  config
}: reducerActionArgs): Promise<Config> => {
  config.pipelines = config.pipelines.filter((pipeline: Pipeline) =>
    pipeline.trigger?.actions?.includes(action)
  );
  return config;
};

const getPipes = (config: Config) => {
  const pipelines = config.pipelines.map((pipeline: Pipeline) => pipeline.name);
  pipelines.sort();
  for (const pipe of pipelines) {
    console.log(pipe);
  }
};

export {
  useConfig,
  useStore,
  getPipes,
  defineConfig,
  Config,
  getActions,
  reducerName,
  hasNoTrigger,
  hasTrigger,
  reducerAction,
  reducerBranch
};
