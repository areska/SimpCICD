import { log } from "@composables/logger";
import { useExec } from "@composables/exec";
import {
  reducerName,
  hasNoTrigger,
  hasTrigger,
  reducerBranch,
  reducerAction
} from "@composables/config";
import { getBranch } from "@utils/git";
import { fork } from "@utils/forker";

import type { Config, Pipeline, Action } from "@def/types";
import lodash from "lodash";

export const useTrigger = (config: Config) => {
  const { execPipeline } = useExec();

  const trigger = async (name?: string, options?: any) => {
    const hasName = await reducerName({
      name: name,
      config: lodash.clone(config)
    });
    const withoutTrigger = await hasNoTrigger({
      name: name,
      config: lodash.clone(hasName)
    });
    const withTrigger = await hasTrigger({
      name: name,
      config: lodash.clone(hasName)
    });
    const hasBranch = await reducerBranch({
      name: name,
      config: lodash.clone(withTrigger)
    });
    const pipelines = lodash.uniq([
      ...withoutTrigger?.pipelines,
      ...hasBranch?.pipelines
    ]);
    for (const pipeline of pipelines) {
      try {
        if (options && options.spawn) {
          log.debug(`Running pipeline ${name} in background...`);
          await fork({ pipeline: name });
        } else {
          await execPipeline(pipeline);
        }
      } catch (err) {
        return err;
      }
    }
  };

  const bulkTrigger = async (action: Action) => {
    const hasBranch = await reducerBranch({
      config: lodash.clone(config)
    });
    const hasAction = await reducerAction({
      action: action,
      config: config
    });
    const pipelines = hasAction?.pipelines;
    for (const pipeline of hasAction.pipelines) {
      try {
        await execPipeline(pipeline);
      } catch (err) {
        return err;
      }
    }
  };

  return {
    trigger,
    bulkTrigger
  };
};
