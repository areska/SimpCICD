import cp from "child_process";
import { log } from "@composables/logger";
import { getGitPath, getBranch } from "@utils/git";
import type { Action } from "@def/types";
import { useStore } from "@composables/config";

interface ForkOptions {
  action?: Action;
  pipeline?: string;
}

export const fork = async ({ action, pipeline }: ForkOptions) => {
  const store = useStore();
  const gitRoot = await getGitPath();
  const target = `${gitRoot}/node_modules/simpcicd/dist/bin/caller.js`;
  const argv: string[] = [];
  if (!!action) {
    argv.push(action as string);
  }
  if (!!pipeline) {
    const actualBranch = await getBranch();
    argv.push(actualBranch);
    argv.push(pipeline);
  }
  try {
    const subprocess = cp.spawn(target, argv, {
      detached: true,
      cwd: store.root as string,
      stdio: ["ignore", "ignore", "ignore"]
      // stdio: "inherit"
    });
    subprocess.unref();
  } catch (err) {
    log.warn(err);
  }
  return;
};
