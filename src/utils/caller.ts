import { useTrigger } from "@resolvers/trigger";
import { useExec } from "@composables/exec";
import type { Config, Action } from "@def/types";
import {
  reducerName,
  hasNoTrigger,
  hasTrigger,
  reducerBranch,
  reducerAction
} from "@composables/config";
import lodash from "lodash";

interface Args {
  action: Action;
  config: Config;
  pipeline?: string;
}

export const call = async ({ config, action, pipeline }: Args) => {
  const { execPipeline } = useExec();
  const { bulkTrigger, trigger } = useTrigger(config);
  if (!!pipeline) {
    await trigger(pipeline);
  } else {
    await bulkTrigger(action);
  }
};
